package com.mycompany.app;

public class Field {
    private Type type;

    public Field(Type type) {
        this.type = type;
    }

    public Type getType() {
        return this.type;
    }

    public enum Type {
        PROPERTY,
        START,
        CHANCE,
        GO_TO_JAIL,
        ON_JAIL_VISIT,
        FREE_PARKING
    }

    public boolean equals(Field other) {
        return this.type == other.type;
    }
}
