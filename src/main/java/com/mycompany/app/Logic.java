package com.mycompany.app;

import java.lang.reflect.Array;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Deque;
import java.util.List;
import java.util.Random;

public class Logic {
    private List<Player> players;
    private List<Field> board;
    private Deque<ChanceCard> deck;
    private Random random = new Random();
    private boolean gameIsOver;
    private int currentPlayerIndex;

    private static final Logic logic = new Logic();

    // Singleton guaranteed private
    private Logic() {
    }

    public void applyGetPlayerOutOfJail(boolean usedFreeOutOfJailCard) {
        Player player = players.get(getCurrentPlayerIndex());
        if (!player.getIsInJail()) {
            throw new IllegalStateException("Player must be in jail before calling this function.");
        }
        if (usedFreeOutOfJailCard) {
            if (player.getHasFreeOutOfJail()) {
                player.setHasFreeOutOfJail(false);
                player.setIsInJail(false);
                putUsedCardBack(ChanceCard.OUT_OF_JAIL);
            } else {
                throw new IllegalStateException("Player cannot use free out of jail card, if they do not possess it.");
            }
        } else {
            setMoney(player, player.getMoney() - 1);
            player.setIsInJail(false);
        }
    }

    // (Decisions, isFreeProperties)
    public Pair<List<Integer>, Boolean> getMustMoveDecisions(){
        Player player = players.get(getCurrentPlayerIndex());
        if (!player.getHasMustMoveCard()) {
            throw new IllegalStateException("You can only call this method if the current player has a must-move card");
        }
        List<Integer> res = new ArrayList<>();
        boolean isNonOwnedProperty = true;
        for (int i = 0; i < board.size(); ++i) {
            Property tmp = castToPropertyOrNull(board.get(i));
            if (tmp != null && tmp.getOwnedBy() == Player.NONE_ID) {
                res.add(i);
            }
        }
        if (res.size() == 0) {
            isNonOwnedProperty = false;
            for (int i = 0; i < board.size(); ++i) {
                if (board.get(i).getType() == Field.Type.PROPERTY) {
                    res.add(i);
                }
            }
        }
        return new Pair<>(res, isNonOwnedProperty);
    }


    public boolean isGameOver() {
        return gameIsOver;
    }

    public static Logic getSingleton() {
        return logic;
    }

    // Immutable getters of players and board lists, so that the user has to
    // make explicit calls to logic in order to modify the state of logic
    public List<Player> getPlayers() {
        return new ArrayList<>(players);
    }


    public int getCurrentPlayerIndex() {
        if (currentPlayerIndex == -1) {
            throw new IllegalStateException("Turn has not been started, so no player index exists.");
        }
        return currentPlayerIndex;
    }

    public List<Field> getBoard() {
        return new ArrayList<>(board);
    }

    public void reset(int nrOfPlayers) {
        currentPlayerIndex = -1;
        gameIsOver = false;
        if (nrOfPlayers > 4 || nrOfPlayers < 2) {
            throw new IllegalArgumentException(
                    "Amount of players must be [2;4]");
        }

        int startMoney = -1;
        if (nrOfPlayers == 2) {
            startMoney = 20;
        } else if (nrOfPlayers == 3) {
            startMoney = 18;
        } else if (nrOfPlayers == 4) {
            startMoney = 16;
        }

        // Setting up players
        players = new ArrayList<>();
        for (int i = 0; i < nrOfPlayers; ++i) {
            players.add(new Player(i, 0, startMoney));
        }

        // Setting up board
        board = new ArrayList<>();

        // clang-format off
        board.add(new Field(Field.Type.START));
        board.add(new Property(1, Property.Color.BROWN, "Burgerbaren"));
        board.add(new Property(1, Property.Color.BROWN, "Pizzeriaet"));
        board.add(new Field(Field.Type.CHANCE));
        board.add(new Property(1, Property.Color.BRIGHT_BLUE, "Slikbutikken"));
        board.add(new Property(1, Property.Color.BRIGHT_BLUE, "Iskiosken"));
        board.add(new Field(Field.Type.ON_JAIL_VISIT));
        board.add(new Property(2, Property.Color.PINK, "Museet"));
        board.add(new Property(2, Property.Color.PINK, "Biblioteket"));
        board.add(new Field(Field.Type.CHANCE));
        board.add(new Property(2, Property.Color.ORANGE, "Skaterparken"));
        board.add(new Property(2, Property.Color.ORANGE, "Swimmingpoolen"));
        board.add(new Field(Field.Type.FREE_PARKING));
        board.add(new Property(3, Property.Color.RED, "Spillehallen"));
        board.add(new Property(3, Property.Color.RED, "Biografen"));
        board.add(new Field(Field.Type.CHANCE));
        board.add(new Property(3, Property.Color.YELLOW, "Legetøjsbutikken"));
        board.add(new Property(3, Property.Color.YELLOW, "Dyrehandlen"));
        board.add(new Field(Field.Type.GO_TO_JAIL));
        board.add(new Property(4, Property.Color.GREEN, "Bowlinghallen"));
        board.add(new Property(4, Property.Color.GREEN, "Zoo"));
        board.add(new Field(Field.Type.CHANCE));
        board.add(new Property(5, Property.Color.DARK_BLUE, "Vandlandet"));
        board.add(new Property(5, Property.Color.DARK_BLUE, "Strandpromenaden"));
        // clang-format on

        // Setting up chance deck
        List<ChanceCard> arrayDeck = new ArrayList<>();
        arrayDeck.addAll(Arrays.asList(ChanceCard.values()));
        shuffle(arrayDeck);
        deck = new ArrayDeque<>(arrayDeck);
    }

    public int rollDie() {
        return random.nextInt(6) + 1;
    }

    public Player startNextTurn() {
        currentPlayerIndex = (currentPlayerIndex + 1) % players.size();

        return players.get(currentPlayerIndex);
    }

    // Player is the player after moving
    // startPos is the current player position before moving
    // Player MUST be in a modified different end position
    // Returns a list of conesquences, which do not modify the state of the game
    public List<PlayerConsequence> getConsequenceOfMove(int startPos) {
        Player player = players.get(getCurrentPlayerIndex());
        int endPos = player.getPosition();
        List<PlayerConsequence> res = new ArrayList<>();
        Field currentField = board.get(endPos);
        // Standing on go to jail. Only effect should be to go to jail
        if (currentField.getType() == Field.Type.GO_TO_JAIL) {
            return Arrays.asList(PlayerConsequence.GO_TO_JAIL);
        }

        // Check if the player needs a startsum
        if (endPos < startPos) {
            res.add(PlayerConsequence.START_SUM);
        }

        // If standing on a property owned by another player
        Property propField = castToPropertyOrNull(currentField);

        if (propField != null && propField.getOwnedBy() != Player.NONE_ID &&
                propField.getOwnedBy() != player.getID()) {
            int leftOf = (endPos - 1) % board.size();
            int rightOf = (endPos + 1) % board.size();
            Property leftProp = castToPropertyOrNull(board.get(leftOf));
            Property rightProp = castToPropertyOrNull(board.get(rightOf));
            if ((leftProp != null &&
                    leftProp.getOwnedBy() == propField.getOwnedBy()) ||
                    (rightProp != null &&
                            rightProp.getOwnedBy() == propField.getOwnedBy())) {
                res.add(PlayerConsequence.MUST_PAY_DOUBLY);
            } else {
                res.add(PlayerConsequence.MUST_PAY_SINGLY);
            }
        }

        // If standing on property not owned by anyone, then the player has to
        // buy
        if (currentField.getType() == Field.Type.PROPERTY && propField != null &&
                propField.getOwnedBy() == Player.NONE_ID) {
            res.add(PlayerConsequence.MUST_BUY);
        }

        // If standing on chance field, then the player must draw a card
        if (currentField.getType() == Field.Type.CHANCE) {
            res.add(PlayerConsequence.CHANCE);
        }

        return res;
    }

    private Property castToPropertyOrNull(Field field) {
        return field.getType() == Field.Type.PROPERTY ? (Property) field : null;
    }

    // Applies a list of consequences, modifying the game state.
    // If player draws a card, return the drawn card, else null.
    // Guarantees to work correctly if passed a list of consequences generated
    // from getConsequenceOfMove
    public ChanceCard applyConsequences(List<PlayerConsequence> consequences) {
        Player player = players.get(getCurrentPlayerIndex());
        if (consequences.contains(PlayerConsequence.GO_TO_JAIL)) {
            goToJail(player);
            return null; // Nothing else should happen
        }

        if (consequences.contains(PlayerConsequence.START_SUM)) {
            setMoney(player, player.getMoney() + 2);
        }

        Property payProperty = castToPropertyOrNull(board.get(player.getPosition()));

        if (consequences.contains(PlayerConsequence.MUST_PAY_SINGLY)) {
            Player ownedByPlayer = getPlayerWithID(payProperty.getOwnedBy());
            setMoney(player, player.getMoney() - payProperty.getPrice());
            setMoney(ownedByPlayer, ownedByPlayer.getMoney() + payProperty.getPrice());
        }

        if (consequences.contains(PlayerConsequence.MUST_PAY_DOUBLY)) {
            Player ownedByPlayer = getPlayerWithID(payProperty.getOwnedBy());
            setMoney(player, player.getMoney() - 2 * payProperty.getPrice());
            setMoney(ownedByPlayer, ownedByPlayer.getMoney() + 2 * payProperty.getPrice());
        }

        if (consequences.contains(PlayerConsequence.MUST_BUY)) {
            setMoney(player, player.getMoney() - payProperty.getPrice());
            payProperty.setOwnedBy(player.getID());
        }

        if (consequences.contains(PlayerConsequence.CHANCE)) {
            return drawCard();
        }

        return null;
    }

    private Player getPlayerWithID(int id) {
        for (Player p : players) {
            if (p.getID() == id) {
                return p;
            }
        }
        throw new IllegalArgumentException("No player with such id: " + id);
    }

    // pair.first contains a card if player drew a card as a result of inserted
    // "card" otherwise null. pair.second contains a list of colors if there is
    // an optional (or even single) color case. pair CANNOT be null
    // NrOfMovesWishedFor is relevant for cards where you have the option to
    // move colors, then return a list of colors
    public Pair<ChanceCard, List<Property.Color>>
    processChanceCard(ChanceCard card, int nrOfMovesWishedFor) {
        Player player = players.get(getCurrentPlayerIndex());
        Pair<ChanceCard, List<Property.Color>> pair = null;
        switch (card) {
        case CAR_MOVES_TO_FREE_PROPERTY:
            getPlayerWithID(Player.IDs.CAR.getID()).setHasMustMoveCard(true);
            pair = new Pair<>(drawCard(), null);
            break;
            case MOVE_FORWARD_TO_START:
                player.setPosition(0);
                pair = new Pair<>(null, null);
                putUsedCardBack(card);
                break;
            case MOVE_UP_TO_5_FIELDS:
                if (!(0 <= nrOfMovesWishedFor && nrOfMovesWishedFor <= 5)) {
                    throw new IllegalArgumentException(
                            "Player can only move [0;5], requested for: " +
                                    nrOfMovesWishedFor);
                }
                movePlayer(nrOfMovesWishedFor);
                pair = new Pair<>(null, null);
                putUsedCardBack(card);
                break;
            case FREE_FIELD_ORANGE:
                pair = new Pair<>(null, Arrays.asList(Property.Color.ORANGE));
                putUsedCardBack(card);
                break;
            case MOVE_1_OR_CHANCE:
                if (!(nrOfMovesWishedFor == 0 || nrOfMovesWishedFor == 1)) {
                    throw new IllegalArgumentException(
                            "Moves must either be 0, for no moves, or 1 for a single move, requested for: " +
                                    nrOfMovesWishedFor);
                }
                if (nrOfMovesWishedFor == 0) {
                    pair = new Pair<>(drawCard(), null);
                } else if (nrOfMovesWishedFor == 1) {
                    movePlayer(1);
                    pair = new Pair<>(null, null);
                }
                putUsedCardBack(card);
                break;
            case SHIP_MOVES_TO_FREE_PROPERTY:
                getPlayerWithID(Player.IDs.SHIP.getID()).setHasMustMoveCard(true);
                pair = new Pair<>(drawCard(), null);
                break;
            case TOO_MUCH_CANDY:
                setMoney(player, player.getMoney() - 2);
                pair = new Pair<>(null, null);
                putUsedCardBack(card);
                break;
            case FREE_FIELD_ORANGE_GREEN:
                pair = new Pair<>(null, Arrays.asList(Property.Color.ORANGE,
                        Property.Color.GREEN));
                putUsedCardBack(card);
                break;
            case FREE_FIELD_BRIGHT_BLUE:
                pair = new Pair<>(null, Arrays.asList(Property.Color.BRIGHT_BLUE));
                putUsedCardBack(card);
                break;
            case OUT_OF_JAIL:
                player.setHasFreeOutOfJail(true);
                pair = new Pair<>(null, null);
                break;
            case MOVE_TO_STRANDPROMENADEN:
                player.setPosition(23);
                putUsedCardBack(card);
                break;
            case CAT_MOVES_TO_FREE_PROPERTY:
                getPlayerWithID(Player.IDs.CAT.getID()).setHasMustMoveCard(true);
                pair = new Pair<>(drawCard(), null);
                break;
            case DOG_MOVES_TO_FREE_PROPERTY:
                getPlayerWithID(Player.IDs.DOG.getID()).setHasMustMoveCard(true);
                pair = new Pair<>(drawCard(), null);
                break;
            case ITS_YOUR_BIRTHDAY:
                int acc = 0;
                for (Player p : players) {
                    if (p.getID() != player.getID()) {
                        setMoney(p, p.getMoney() - 1);
                        acc++;
                    }
                }
                setMoney(player, player.getMoney() + acc);
                pair = new Pair<>(null, null);
                putUsedCardBack(card);
                break;
            case FREE_FIELD_PINK_DARK_BLUE:
                pair = new Pair<>(null, Arrays.asList(Property.Color.PINK,
                        Property.Color.DARK_BLUE));
                putUsedCardBack(card);
                break;
            case YOU_DID_ALL_YOUR_HOMEWORK:
                setMoney(player, player.getMoney() + 2);
                pair = new Pair<>(null, null);
                putUsedCardBack(card);
                break;
            case FREE_FIELD_RED:
                pair = new Pair<>(null, Arrays.asList(Property.Color.RED));
                putUsedCardBack(card);
                break;
            case FREE_FIELD_SKATERPARKEN:
                player.setPosition(10);
                Property prop = (Property) board.get(10);
                if (prop.getOwnedBy() == Player.NONE_ID) {
                    prop.setOwnedBy(player.getID());
                }
                // Else let the payment to another player be handled by
                // applyConsequences function
                pair = new Pair<>(null, null);
                putUsedCardBack(card);
                break;
            case FREE_FIELD_BRIGHT_BLUE_RED:
                pair = new Pair<>(null, Arrays.asList(Property.Color.BRIGHT_BLUE,
                        Property.Color.RED));
                putUsedCardBack(card);
                break;
            case FREE_FIELD_BROWN_YELLOW:
                pair = new Pair<>(null, Arrays.asList(Property.Color.BROWN,
                        Property.Color.YELLOW));
                putUsedCardBack(card);
                break;
            default:
                throw new AssertionError("Method should exhaustively handle all possible chance cards, it apparently did not.");
        }


        if (pair == null) {
            throw new AssertionError(
                    "Method is guaranteed to return a non-null pair, but returns null.",
                    new NullPointerException("Pair is not an initialized class."));
        }
        return pair;
    }

    // If the user got a color card, then this will return the valid positions, given the colors
    public List<Integer> getPositionsFromColors(List<Property.Color> colors) {
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < board.size(); ++i) {
            Field current = board.get(i);
            Property property = castToPropertyOrNull(current);
            if (property != null && current.getType() == Field.Type.PROPERTY && colors.contains(property.getColor())) {
                result.add(i);
            }
        }
        return result;
    }

    // This will given a color card list of indexes (generated from getPositionFromColors)
    // then you can choose which player has to move to the choice
    // This can also be used in conjunction with get getMustMoveDecisions.
    public void applyPropertyFromChoices(List<Integer> validPositions, int choice) {
        Player player = players.get(getCurrentPlayerIndex());
        player.setPosition(validPositions.get(choice));
        Property prop = castToPropertyOrNull(board.get(player.getPosition()));
        if (prop != null) {
            if (prop.getOwnedBy() == Player.NONE_ID) {
                setMoney(player, player.getMoney() - prop.getPrice());
                prop.setOwnedBy(player.getID());
            } else if (prop.getOwnedBy() != player.getID()) {
                Player previousOwner = getPlayerWithID(prop.getOwnedBy());
                setMoney(player, player.getMoney() - prop.getPrice());
                setMoney(previousOwner, previousOwner.getMoney() + prop.getPrice());
            } else {
                throw new IllegalStateException("Player should not have the choice to move to his own property.");
            }
        }
    }

    public int movePlayer(int move) {
        Player player = players.get(getCurrentPlayerIndex());
        if (player.getIsInJail()) {
            throw new IllegalStateException("Player cannot be moved while in jail.");
        }
        int pos = player.getPosition();
        int newPos = (pos + move) % board.size();
        player.setPosition(newPos);
        return newPos;
    }

    private void setOwned(Property property, Player player) {
        property.setOwnedBy(player.getID());
    }

    private void setMoney(Player player, int money) {
        player.setMoney(money);
        if (player.getMoney() < 0) {
            gameIsOver = true;
        }
    }

    private void goToJail(Player player) {
        player.setPosition(6);
        player.setIsInJail(true);
    }

    private ChanceCard drawCard() {
        return deck.pollFirst();
    }

    private void putUsedCardBack(ChanceCard card) {
        deck.add(card);
    }

    // Knuth shuffle
    private void shuffle(List<ChanceCard> deck) {
        List<ChanceCard> res = new ArrayList<>();
        for (int i = 0; i < deck.size(); ++i) {
            int choice = random.nextInt(deck.size());
            ChanceCard card = deck.get(choice);
            deck.remove(choice);
            res.add(card);
        }
        deck = res;
    }

    public enum PlayerConsequence {
        // If player stands on non-owned property
        MUST_BUY,
        // Enemy property, only single property owned
        MUST_PAY_SINGLY,
        // Enemy property, where both colors are owned
        MUST_PAY_DOUBLY,
        // Receive a sum when passing START
        START_SUM,
        // Draw a card if landing on Chance
        CHANCE,
        // Go to jail if landing on jail
        GO_TO_JAIL
    }

}
