package com.mycompany.app;

class Property extends Field {
    String name;
    int ownedByID;
    Color color;
    int price;

    public Property(int price, Color color, String name) {
        super(Field.Type.PROPERTY);
        this.ownedByID = Player.NONE_ID;
        this.color = color;
        this.price = price;
        this.name = name;
    }

    public enum Color {
        PINK,
        ORANGE,
        RED,
        YELLOW,
        GREEN,
        DARK_BLUE,
        BROWN,
        BRIGHT_BLUE
    }

    public int getOwnedBy() {
        return ownedByID;
    }

    public void setOwnedBy(int id) {
        this.ownedByID = id;
    }

    public int getPrice() {
        return price;
    }

    public Color getColor() {
        return color;
    }

    public String getName() {
        return name;
    }

    public boolean equals(Field other) {
        if (other.getClass() == Property.class) {
            Property prop = (Property) other;
            return this.getColor() == prop.getColor() &&
                    this.getOwnedBy() == prop.getOwnedBy() &&
                    this.getPrice() == prop.getPrice() &&
                    this.getName() == prop.getName() && super.equals(other);
        }
        return false;
    }
}
