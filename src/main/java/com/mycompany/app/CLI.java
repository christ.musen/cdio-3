package com.mycompany.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CLI {

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_RED = "\u001B[91m";
    public static final String ANSI_GREEN = "\u001B[92m";
    public static final String ANSI_ORANGE = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[94m";
    public static final String ANSI_PURPLE = "\u001B[95m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_YELLOW = "\u001B[93m";
    public static final String ANSI_BROWN = "\u001B[90m";

    private static String[] fieldColors;

    private List<prettyPrintField> printFields;

    public Scanner scan = new Scanner(System.in);

    private List<Field> board;
    private List<Player> players;
    private Logic logic;

    public CLI() {
        initializeColors();
        logic = Logic.getSingleton();
        logic.reset(4);
        refresh();
    }

    public void run() {
        welcomeMessage();
        // Print board
        // Print current player turn

        System.out.println("Spillet er nu startet med 4 spillere. Held og lykke!");

        while (true) {
            clearScreen();
            printState();
            gameLoop();

            if (logic.isGameOver()) {
                break;
            }

            System.out.println("Skriv P for at se alle spilleres penge\nSkriv N for næste tur.");
            endTurn();
        }
        endgameScreen();
    }

    public Player findMaxPlayer(List<Player> players) {
        Player max = players.get(0);
        for (Player p : players) {
            if (p.getMoney() > max.getMoney()) {
                max = p;
            }
        }
        return max;
    }

    public List<Player> sortPlayersByMoney() {
        List<Player> players = logic.getPlayers();
        List<Player> sorted = new ArrayList<>();
        Player max;
        for (int i = 0; i < logic.getPlayers().size(); i++) {
            max = findMaxPlayer(players);
            sorted.add(max);
            players.remove(max);
        }
        return sorted;
    }

    public void endgameScreen() {
        System.out.println("Spillet er ovre!");
        System.out.println("Stillingen sluttede som følger;");
        List<Player> players = sortPlayersByMoney();
        int position = 1;
        for (Player p : players) {

            System.out.println(position + ". " + fieldColors[p.getID() + 2] + Player.Names.values()[p.getID() + 1].getName() + ANSI_RESET
                    + " med " + p.getMoney() + "M!");
            position++;

        }
    }


    public void endTurn(){
        boolean nextTurn = false;
        String s;
        while (!nextTurn){
            s = scan.nextLine();
            if (s.equals("P") || s.equals("p")){
                printPlayerMoney();
            }
            else if (s.equals("N") || s.equals("n")) {
                nextTurn = true;
            }
            else {
                System.out.println("Du skrev noget andet end mulighederne. Prøv igen.");
            }
        }
    }

    public void printPlayerMoney(){
        for (Player p : logic.getPlayers()){
            System.out.println(fieldColors[p.getID()+2] + Player.Names.values()[p.getID() + 1].getName() + ANSI_RESET + " har " + p.getMoney() + "M");
        }
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }

    public void jailCheck(Player player) {
        if (player.getIsInJail()) {
            System.out.println("Du er i fængsel!");
            if (player.getHasFreeOutOfJail()) {
                System.out.println("Du havde et \"Get out of jail freecard\". Det er brugt, og du kan nu fortsætte.");
            } else {
                System.out.println("Du havde ikke noget freecard, så du har betalt 1M for at komme ud. Du kan nu fortsætte.");
            }
            logic.applyGetPlayerOutOfJail(player.getHasFreeOutOfJail());
        }
    }

    public int diceroll() {
        int diceroll = -1;
        boolean roll = false;
        String line;
        while (!roll){
            line = scan.nextLine();
            if (line.equals("R") || line.equals("r")){
                diceroll = logic.rollDie();
                roll = true;
            } else if (line.equals("Cheat") || line.equals("cheat")) {
                System.out.println("Din lille snyder! Skriv det tal den næste spiller skal bevæge sig.");
                diceroll = scan.nextInt() % 24;
                line = scan.nextLine();
                roll = true;
            } else {
                System.out.println("Det var ikke et R. Prøv igen!");
            }
        }
        return diceroll;
    }

    public void playerMustMoveCard(Player player) {
        Pair<List<Integer>, Boolean> pair;
        String line;
        if (player.getHasMustMoveCard()) {
            pair = logic.getMustMoveDecisions();

            if (pair.second) {
                System.out.println("Vælg blandt disse uejede ejendomme.");
            } else {
                System.out.println("Der var ikke nogen ledige ejendomme. Vælg en valgfri, og køb den af ejeren.");
            }

            boolean validChoice = false;
            int size = printChoices(pair.first);
            int choice = -1;

            while (!validChoice) {
                line = scan.nextLine();
                try {
                    choice = Integer.parseInt(line);
                    if (choice > 0 && choice <= size) {
                        validChoice = true;
                    } else {
                        System.out.println("Det var ikke en mulighed. Prøv igen.");
                    }
                } catch (Exception e) {
                    System.out.println("Det var ikke en mulighed. Prøv igen.");
                }
            }
            logic.applyPropertyFromChoices(pair.first, choice);
        }
    }


    public void gameLoop() {
        String line;
        int diceroll;
        Player currentPlayer = logic.startNextTurn();
        System.out.println("Det er " + Player.Names.values()[currentPlayer.getID() + 1].getName() + "s tur!\nTryk R for at rulle med terningen!");

        jailCheck(currentPlayer);

        playerMustMoveCard(currentPlayer);

        diceroll = diceroll();

        System.out.println("Du slog en " + diceroll + "'er");
        int startPosition = currentPlayer.getPosition();
        logic.movePlayer(diceroll);
        List<Logic.PlayerConsequence> consequences = logic.getConsequenceOfMove(startPosition);
        ChanceCard chanceCard = logic.applyConsequences(consequences);
        if (consequences.contains(Logic.PlayerConsequence.GO_TO_JAIL)){
            System.out.println("Øv! Du landede på \"Gå i fængsel\", og er rykket direkte dertil!");
        }
        if (consequences.contains(Logic.PlayerConsequence.START_SUM)){
            System.out.println("Du passerede start, og har modtaget 2M!");
        }
        if (consequences.contains(Logic.PlayerConsequence.CHANCE)){
            boolean validChoice = false;
            int choice = -1;
            while (chanceCard != null){
                Pair<ChanceCard, List<Property.Color>> pair = null;
                System.out.println("Du trak et chancekort! Det siger: \n");

                if (chanceCard == ChanceCard.MOVE_1_OR_CHANCE){
                    System.out.println("Ryk 1 felt frem, eller træk et nyt chancekort.");
                    System.out.println("Skriv 1 for at rykke frem, eller 0 for at blive på samme felt og få et nyt kort.");
                    while (!validChoice){
                        line = scan.nextLine();
                        if (line.equals("0")){
                            chanceCard = logic.processChanceCard(chanceCard, 0).first;
                            validChoice = true;
                        } else if (line.equals("1")) {
                            int startpos = currentPlayer.getPosition();
                            chanceCard = logic.processChanceCard(chanceCard, 1).first;
                            consequences.addAll(logic.getConsequenceOfMove(startpos));
                            validChoice = true;
                        } else {
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                }
                if (chanceCard == ChanceCard.MOVE_UP_TO_5_FIELDS){
                    System.out.println("Ryk op til 5 felter fremad.");
                    System.out.println("Skriv et tal mellem 0 og 5.");
                    while (!validChoice){

                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                        if (choice >= 0 && choice <= 5){
                            int startpos = currentPlayer.getPosition();
                            chanceCard = logic.processChanceCard(chanceCard, choice).first;
                            consequences.addAll(logic.getConsequenceOfMove(startpos));
                            validChoice = true;
                        } else {
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                }
                if (chanceCard == ChanceCard.CAR_MOVES_TO_FREE_PROPERTY){
                    System.out.println("Giv dette kort til Bilen. Næste tur starter han med at køre til en valgfri ledig ejendom og købe det. "+
                            "Hvis ingen felter er ledige skal du købe et af en anden spiller.\nTræk et nyt kort.");
                    chanceCard = logic.processChanceCard(chanceCard,0).first;
                }
                if (chanceCard == ChanceCard.CAT_MOVES_TO_FREE_PROPERTY){
                    System.out.println("Giv dette kort til Katten. Næste tur starter han med at liste til en valgfri ledig ejendom og købe det. "+
                            "Hvis ingen felter er ledige skal du købe et af en anden spiller.\nTræk et nyt kort.");
                    chanceCard = logic.processChanceCard(chanceCard,0).first;
                }
                if (chanceCard == ChanceCard.DOG_MOVES_TO_FREE_PROPERTY){
                    System.out.println("Giv dette kort til Hunden. Næste tur starter han med at løbe til en valgfri ledig ejendom og købe det. "+
                            "Hvis ingen felter er ledige skal du købe et af en anden spiller.\nTræk et nyt kort.");
                    chanceCard = logic.processChanceCard(chanceCard,0).first;
                }
                if (chanceCard == ChanceCard.SHIP_MOVES_TO_FREE_PROPERTY){
                    System.out.println("Giv dette kort til Skibet. Næste tur starter han med at sejle til en valgfri ledig ejendom og købe det. "+
                            "Hvis ingen felter er ledige skal du købe et af en anden spiller.\nTræk et nyt kort.");
                    chanceCard = logic.processChanceCard(chanceCard,0).first;
                }
                if (chanceCard == ChanceCard.FREE_FIELD_BRIGHT_BLUE){
                    System.out.println("Ryk til en valgfri lyseblå ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_BRIGHT_BLUE_RED){
                    System.out.println("Ryk til en valgfri lyseblå eller rød ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_BROWN_YELLOW){
                    System.out.println("Ryk til en valgfri brun eller gul ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_ORANGE){
                    System.out.println("Ryk til en valgfri orange ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_ORANGE_GREEN){
                    System.out.println("Ryk til en valgfri orange eller grøn ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_PINK_DARK_BLUE){
                    System.out.println("Ryk til en valgfri pink eller blå ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_RED){
                    System.out.println("Ryk til en valgfri rød ejendom. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    pair = logic.processChanceCard(chanceCard, 0);
                    chanceCard = pair.first;
                    int size = printChoices(logic.getPositionsFromColors(pair.second));
                    while (!validChoice){
                        line = scan.nextLine();
                        try {
                            choice = Integer.parseInt(line);
                            if (choice > 0 && choice <= size){
                                validChoice = true;
                            } else {
                                System.out.println("Det var ikke en mulighed. Prøv igen.");
                            }
                        } catch (Exception e){
                            System.out.println("Det var ikke en mulighed. Prøv igen.");
                        }
                    }
                    logic.applyPropertyFromChoices(logic.getPositionsFromColors(pair.second), choice);
                }
                if (chanceCard == ChanceCard.FREE_FIELD_SKATERPARKEN){
                    System.out.println("Ryk til Skaterparken. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
                if (chanceCard == ChanceCard.ITS_YOUR_BIRTHDAY){
                    System.out.println("Det er din fødselsdag! Modtag 1M fra alle andre spillere.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
                if (chanceCard == ChanceCard.MOVE_FORWARD_TO_START){
                    System.out.println("Ryk frem til Start.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
                if (chanceCard == ChanceCard.MOVE_TO_STRANDPROMENADEN){
                    System.out.println("Ryk til Strandpromenaden. Er den ledig får du den gratis, ellers betal leje til ejeren.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
                if (chanceCard == ChanceCard.OUT_OF_JAIL){
                    System.out.println("Du kommer gratis ud af fængslet næste gang du kommer bag tremmer.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
                if (chanceCard == ChanceCard.TOO_MUCH_CANDY){
                    System.out.println("Du har spist for meget slik! Betal 2M til banken.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
                if (chanceCard == ChanceCard.YOU_DID_ALL_YOUR_HOMEWORK){
                    System.out.println("Du har klaret alle dine lektier! Modtag 2M fra banken.");
                    chanceCard = logic.processChanceCard(chanceCard, 0).first;
                }
            }
        }
        if (consequences.contains(Logic.PlayerConsequence.MUST_BUY)){
            Property prop = (Property) board.get(currentPlayer.getPosition());
            System.out.println("Du landede på " + prop.getName() + " og har betalt " + prop.getPrice() + "M for at købe den!\n" +
                    "Du har nu " + currentPlayer.getMoney() + "M tilbage!");
        }
        if (consequences.contains(Logic.PlayerConsequence.MUST_PAY_SINGLY)){
            Property prop = (Property) board.get(currentPlayer.getPosition());
            System.out.println("Du er landet på " + prop.getName() + " som er ejet af " + Player.Names.values()[prop.getOwnedBy()+1].getName()
                    + "\n Du har betalt " + prop.getPrice() + "M, og har nu " + currentPlayer.getMoney() + "M tilbage!" );
        }
        if (consequences.contains(Logic.PlayerConsequence.MUST_PAY_DOUBLY)){
            Property prop = (Property) board.get(currentPlayer.getPosition());
            System.out.println("Du er landet på " + prop.getName() + " som er ejet af " + Player.Names.values()[prop.getOwnedBy()+1].getName() + ", som også ejer nabofeltet."
                    + "\n Du har betalt " + (prop.getPrice()*2) + "M, og har nu " + currentPlayer.getMoney() + "M tilbage!" );
        }
        if (currentPlayer.getPosition() == 12){
            System.out.println("Du er landet på gratis parkering! Tag en pause.");
        }
        if (currentPlayer.getPosition() == 6 && !consequences.contains(Logic.PlayerConsequence.GO_TO_JAIL)){
            System.out.println("Du er på besøg i fængslet.");
        }
    }

    public void welcomeMessage() {
        System.out.println("Welcome to Matador Junior terminal edition!");
    }

    // Get fresh state of logic model
    public void refresh() {
        this.board = logic.getBoard();
        this.players = logic.getPlayers();
    }

    public void printState() {

        createPrettyPrint();

        for (prettyPrintField ppf : printFields) {

            System.out.print("[");

            if (ppf.getField().getType() == Field.Type.PROPERTY) {
                Property ppfp = (Property)ppf.getField();

                System.out.print(
                    fieldColors[ppfp.getColor().ordinal()] + ppfp.getName() +
                    ANSI_RESET + " " + ppfp.getPrice() + "M"
                    + ", Ejet af " +
                    Player.Names.values()[ppfp.getOwnedBy() + 1].getName());
            } else if (ppf.getField().getType() == Field.Type.START) {
                System.out.print("START");
            } else if (ppf.getField().getType() == Field.Type.CHANCE) {
                System.out.print("¿CHANCE?");
            } else if (ppf.getField().getType() == Field.Type.GO_TO_JAIL) {
                System.out.print("GO TO JAIL");
            } else if (ppf.getField().getType() == Field.Type.ON_JAIL_VISIT) {
                System.out.print("JAIL / VISIT");
            } else if (ppf.getField().getType() == Field.Type.FREE_PARKING) {
                System.out.print("FREE REAL ESTATE");
            }

            for (Player p : ppf.getPlayers()) {
                System.out.print(", " + fieldColors[p.getID()+2] + Player.Names.values()[p.getID() + 1].getName() + ANSI_RESET);
            }
            System.out.println("]");
        }
    }

    public int printChoices(List<Integer> positions) {
        int i = 0;
        for (Integer pos : positions){
            Property prop = (Property) board.get(pos);
            System.out.println("Skriv " + (i+1) + " for at gå til " +
                    fieldColors[prop.getColor().ordinal()] +
                    prop.getName() + ANSI_RESET);
            i++;
        }
        return i;
    }

    public void createPrettyPrint() {
        printFields = new ArrayList<prettyPrintField>();

        prettyPrintField pretty;

        for (Field field : board) {

            pretty = new prettyPrintField(field);
            printFields.add(pretty);
        }

        for (Player p : players) {
            printFields.get(p.getPosition()).players.add(p);
        }
    }

    class prettyPrintField {
        Field field;
        List<Player> players;

        public prettyPrintField(Field field) {
            this.field = field;
            players = new ArrayList<Player>();
        }

        public Field getField() {
            return field;
        }
        public List<Player> getPlayers() {
            return players;
        }
    }

    public void initializeColors() {
        fieldColors = new String[8];

        fieldColors[0] = ANSI_PURPLE;
        fieldColors[1] = ANSI_ORANGE;
        fieldColors[2] = ANSI_RED;
        fieldColors[3] = ANSI_YELLOW;
        fieldColors[4] = ANSI_GREEN;
        fieldColors[5] = ANSI_BLUE;
        fieldColors[6] = ANSI_BROWN;
        fieldColors[7] = ANSI_CYAN;
    }

    public void colorTest(){
        System.out.println(fieldColors[0] +
                "Purple" + fieldColors[1] +
                "Orange" + fieldColors[2] +
                "Red" + fieldColors[3] +
                "Yellow" + fieldColors[4] +
                "Green" + fieldColors[5] +
                "Blue" + fieldColors[6] +
                "Brown" + fieldColors[7] +
                "Cyan" + ANSI_RESET);
    }

}
