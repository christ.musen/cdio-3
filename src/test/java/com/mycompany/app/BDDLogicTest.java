package com.mycompany.app;

import junit.framework.TestCase;

import java.util.List;

public class BDDLogicTest extends TestCase {

    public void testStartGame(){
        System.out.println("When i start a new game with 4 players");
        Logic logic = Logic.getSingleton();
        logic.reset(4);
        System.out.println("I expect the players to start with 16M");
        for (Player p : logic.getPlayers()){
            assertEquals(p.getMoney(),16);
        }
        System.out.println("And after the first player rolls a 4, i expect him to land on a property he is forced to buy.");
        assertTrue(logic.getBoard().get(4).getType() == Field.Type.PROPERTY);
        System.out.println("After he pays up the 1M, i expect him to have 15M money left, and that the property is now owned by him.");
        logic.startNextTurn(); // 0
        logic.movePlayer(4); // 0
        List<Logic.PlayerConsequence> consequences = logic.getConsequenceOfMove(0);
        logic.applyConsequences(consequences);
        assertEquals(logic.getPlayers().get(0).getMoney(), 15);

        Property prop = (Property) logic.getBoard().get(4);

        assertTrue(prop.getOwnedBy() == logic.getPlayers().get(0).getID());

    }






}